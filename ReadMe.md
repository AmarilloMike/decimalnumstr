DecimalNumStr - Repository

### Source Code Repository

The source code for this project is located at:

 https://bitbucket.org/AmarilloMike/decimalnumstr/src


### Project Status

** This is a work in progress !!!! **

This series of numeric and number string utilities
was written in the Go Programming Language ('golang')

The library currently consists of three source files.

1. 'numstrdto.go' contains source code for the NumStrDto type.
This type, and it's associated methods may be used to parse,
format and manage number strings.

2. 'decimal.go' contains source code for the Decimal type. Decimal
types may be used to perform math operations on very large numbers
with a high degree of precision and accuracy.

3. 'numstrutility.go' contains source code for a general purpose library
used to perform common numeric operations and conversions.